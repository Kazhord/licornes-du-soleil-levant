
var mapX;
var mapY;
var cptTemp =0;


/*
var mapLigne =[];
var mapGenerale = []; 
for (i=0; i<mapX; i++){
    mapLigne =[];
    for (j=0; j<mapY; j++){
        mapLigne.push({id:cptTemp, difficulte_terrain:10, occupe:false , posX:i , posY:j});
        cptTemp++;
    }
    mapGenerale.push(mapLigne);
} 
*/
var grid=[];

var mapGenerale = [];

//fonction permettant de convertir la table objet par une table classique de 0 et 1 pour les algos   
function convertirMatrixAngularToSimpleArray(matrixComplete){
   var arrayGeneral = [];

   matrixComplete.forEach(function (row){
        // je reinitialise le array car on change de row
        var arrayRow =[];
        row.forEach (function(value){
            if(!value.occupe){
                arrayRow.push(0);
            }else{
                arrayRow.push(1);
            }
        });
        //sortie de la row on add à la generale
        arrayGeneral.push(arrayRow);
   });
   arrayGeneral = arrayGeneral.transpose();
   return arrayGeneral;
      
}

function nettoyer_map(){
    $("#matrice td").removeClass( "path" );
}

//dessiner tracé a partir du DOM
function drawPath(listPath,SimuFactory) {

    //le premier robot le plus proche go sur le feu puis le suivant etc
    // boucle sur tous les parcours pour voir les plus robots les plus proches 
    for(i=0;i<listPath.length;i++){
        //alert(listPath[i].length);
        //console.log(JSON.stringify(listPath[i]));
        listPath.sort(function(a, b){
          return a.length - b.length; 
        });
         //console.log(JSON.stringify(listPath[i]));
    }
    
    var current =[];
         
    for(i=0;i<listPath.length;i++){
        // liste
        //remove le dernier qui est le feu
        listPath[i].pop();
        
        current[0] = (listPath[i])[0][0];
        current[1] = (listPath[i])[0][1];
  
        for (j=0; j<listPath[i].length; j++) {
            // chaque element
            var posXold = current[0];
            var posYold = current[1];
            current = listPath[i][j];

            var tempCalcul = (current[1]*mapX+current[0]);
            console.log(tempCalcul);
            //var point = document.getElementById(tempCalcul);
            //point.className = point.className + (' path');
            SimuFactory.addLog({type:'robotDeplacement', detail:{timer:1, timestamp:1, robotID:i, oldPosX:posXold, oldPosY:posYold, newPosX:current[0], newPosY:current[1]}});
        }
    }     
}

  
function createMapWithoutMatrix(lengthX,lengthY){
    grid = new PF.Grid(lengthX, lengthY);
}   
  

function createMapWithMatrix(lengthX, lengthY, matrix){
    grid = new PF.Grid(lengthX, lengthY , matrix);
}

//map pour algo
function removeElementOnMap(x,y,grid,matrix){ 
     //maj sur la map algo
    grid.setWalkableAt(x, y, true); 
    
    //map angular
    for(i=0; i<map.length-1; i++){
        for(j=0; j<map[i].length-1; j++){
           if(map[i][j].posX == x && map[i][j].posY == y){
                map[i][j].occupe = false;
           }
        }
    }
}

//fonction permettant de convertir l'etat OCCUPE de la map d'algo à la map angularJS
function blockElementOnMap(x,y,grid,map){
    //maj sur la map algo
    //console.log(grid);
    grid.setWalkableAt(x,y,false); 
    
   // map angular
    for(i=0; i<map.length-1; i++){
        for(j=0; j<map[i].length-1; j++){
           if(map[i][j].posX == x && map[i][j].posY == y){
                map[i][j].occupe = true;
           }
        }
    }
} 

function getElementByPosition(x,y){
    var point = document.getElementById((y*mapX)+x);
    return point;
} 

//initialise un element sur la map et retourne le path entre position de départ et d'arrivée
function initElementOnMap(xD,yD,xA,yA,matrix,finder){
    createMapWithMatrix(mapX,mapY,matrix);
    blockElementOnMap(xD,yD,grid,mapGenerale);
    var path = finder.findPath(xD, yD, xA, yA, grid);
    blockElementOnMap(0,1,grid,mapGenerale);
    /*
    var point = getElementByPosition(xD,yD);
    point.className = point.className + (' start');
    var point = getElementByPosition(yA,xA);
    point.className = point.className + (' end');
    */
    return path;
}
        


simuApp.controller('ControleurAlgo' , function ($scope,SimuFactory) {
    $scope.matrixComplete = getMatriceDifficulte();
    console.log($scope.matrixComplete)
    matrix = convertirMatrixAngularToSimpleArray($scope.matrixComplete);

    $scope.selectAction = function() {
        $scope.matrixComplete = getMatriceDifficulte();
        console.log($scope.matrixComplete)
        matrix = convertirMatrixAngularToSimpleArray($scope.matrixComplete);
        //Scopes.store('ControleurAlgo',$scope);

        mapX = getTaille()[0];
        mapY = getTaille()[1];
        mapGenerale = getMatriceDifficulte();


        var selected_header = $scope.select_algo;

        switch (selected_header) {
            case 'a_star':
                var finder = new PF.AStarFinder();
            break;

            case 'disjkra':
                finder = new PF.DijkstraFinder();   
            break;

            case 'a_star_diag':
                finder = new PF.AStarFinder({
                     allowDiagonal: true
                });
            break;

            case 'disjkra_diag':
                finder = new PF.DijkstraFinder({
                    allowDiagonal: true
                });               
            break;

        }
        
        var pathList = [];

        
        SimuFactory.resetLogs();
        nettoyer_map();
        

        //blockElementOnMap(2,1,grid,$scope.matrixComplete);
        path = initElementOnMap(0,0,18,18,matrix,finder);
        pathList.push(path);
        //console.log(JSON.stringify(path));

        path = initElementOnMap(2,2,16,16,matrix,finder);
        pathList.push(path);

        path = initElementOnMap(1,1,10,10,matrix,finder);
        pathList.push(path); 

        path = initElementOnMap(3,3,7,7,matrix,finder);
        pathList.push(path);  
        
        drawPath(pathList,SimuFactory);
         
    }
});

