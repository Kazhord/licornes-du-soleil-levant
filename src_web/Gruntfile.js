module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-line-remover');
    grunt.loadNpmTasks('grunt-ng-annotate');

    // Permet de configurer les plugins et tâches
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
		
		clean: {
			dist: {
				src: ["dist"]
			},
			after_copy: {
				src: ["dist/dist", "dist/node_modules", "dist/Gruntfile.js", "dist/package.json"]
			},
			remove_assets_components: {
				src: ["dist/assets/app_components", "dist/assets/bower_components"]
			},
			remove_css_js_non_min: {
				src: ["dist/assets/css/ceaseFire.css", "dist/assets/js/ceaseFire.js"]
			}
		}, 

		mkdir: {
			dist: {
				options: {
					create: ['dist']
				}
			},			
			mkdir_assets_css_js: {
				options: {
					create: ['dist/assets/css', 'dist/assets/js']
				}
			}
		},

		copy: {
			files: {
				cwd: '.',  		// set working folder / root to copy
				src: '**/*',    // copy all files and subfolders
				dest: 'dist',   // destination folder
				expand: true    // required when using cwd
			}
		},
		
		concat: {
			css: {
				src: [
					  'dist/assets/bower_components/bootstrap/dist/css/bootstrap.min.css',
					  'dist/assets/bower_components/onepage-scroll/onepage-scroll.css',
					  'dist/assets/bower_components/font-awesome/css/font-awesome.min.css',
					  'dist/assets/app_components/css/general.css',
					  'dist/assets/app_components/css/matrice.css'
					 ],
				dest: 'dist/assets/css/ceaseFire.css'
			},
			js: {
			    options: {
			      separator: ';',
			    },
				src: [

					  'dist/assets/app_components/js/matrice.js',
					  'dist/assets/app_components/js/transpose.js',
					  'dist/assets/app_components/js/pathfinding-browser.min.js',
					  'dist/assets/app_components/js/general.js',
					  'dist/assets/bower_components/bootstrap/dist/js/bootstrap.min.js',
					  'dist/assets/bower_components/onepage-scroll/jquery.onepage-scroll.min.js',
					  'dist/assets/bower_components/angular-route/angular-route.min.js.js',
					  'dist/assets/bower_components/angular/angular.min.js',
					  'dist/assets/bower_components/jquery/jquery.min.js'
					 ],
				dest: 'dist/assets/js/ceaseFire.js'
			}
		},

		uglify: {
			my_target: {
				options: {
					mangle: false
				},
				files: {
					'dist/assets/js/ceaseFire.min.js': ['dist/assets/js/ceaseFire.js']
				}
			}
		}, 

		cssmin: {
			target: {
				files: {
					'dist/assets/css/ceaseFire.min.css': ['dist/assets/css/ceaseFire.css']
				}
			}
		}, 

		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'dist/application/views/app.php': 'dist/application/views/app.php'
				}
			}
		},

		lineremover: {
			css: {
				files: {
					'dist/application/views/app.php': 'dist/application/views/app.php'
				},
				options: {
					exclusionPattern: /rel="stylesheet"/g
				}
			},
			js: {
				files: {
					'dist/application/views/app.php': 'dist/application/views/app.php'
				},
				options: {
					exclusionPattern: /type="text\/javascript"/g
				}
			}
		},

		replace: {
		  css: {
		    src: ['dist/application/views/app.php'],
		    overwrite: true,
		    replacements: [{
		      from: /<!-- CSS_MIN -->/,
		      to: '<link rel="stylesheet" href="assets/css/ceaseFire.min.css">'
		    }]
		  },
		  js: {
		    src: ['dist/application/views/app.php'],
		    overwrite: true,
		    replacements: [{
		      from: /<!-- JS_MIN -->/,
		      to: '<script type="text/javascript" src="assets/js/ceaseFire.min.js"></script>'
		    }]
		  }
		},

		ngAnnotate: {
	        options: {
	            singleQuotes: false,
	        },
	        app1: {
	            files: {
	                'dist/assets/app_components/js/general.js': ['dist/assets/app_components/js/general.js'],
	                'dist/assets/app_components/js/matrice.js': ['dist/assets/app_components/js/matrice.js']
	            }
	        }
	    }

    });

    // Crée la tâche 'default' qui sera exécutée si on ne spécifie aucune tâche
    // Le 2e paramètre indique les tâches qui seront exécutées
    grunt.registerTask('default', 
    	[
    		'clean:dist',
    		'mkdir:dist',
    		'copy',
    		'ngAnnotate',
    		'lineremover:css',
    		'lineremover:js',
    		'replace:css',
    		'replace:js',
    		'clean:after_copy',
    		'mkdir:mkdir_assets_css_js',
    		'concat:css',
    		'concat:js',
    		'clean:remove_assets_components',
    		'uglify',
    		'cssmin',
    		'clean:remove_css_js_non_min', 
    		'htmlmin'
    	]
    );
};



/*

    		'clean:dist',
    		'mkdir:dist',
    		'copy',
    		'ngAnnotate',
    		'lineremover:css',
    		'lineremover:js',
    		'replace:css',
    		'replace:js',
    		'clean:after_copy',
    		'mkdir:mkdir_assets_css_js',
    		'concat:css',
    		'concat:js',
    		'clean:remove_assets_components',
    		'uglify',
    		'cssmin',
    		'clean:remove_css_js_non_min', 
    		'htmlmin'

*/