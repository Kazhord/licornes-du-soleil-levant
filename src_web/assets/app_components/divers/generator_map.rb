map_x = 20-1
map_y = 20-1

if ARGV[0] != nil then
	map_x = ARGV[0].to_i-1
end

if ARGV[1] != nil then
	map_y = ARGV[1].to_i-1
end


cpt=0

puts "["

0.upto(map_x) do |x|
	puts "	["
	0.upto(map_y) do |y|
                        
        puts "		{id:#{cpt}, difficulte_terrain:10, occupe:false , posX:#{x} , posY:#{y}}," if y != map_y
        puts "		{id:#{cpt}, difficulte_terrain:10, occupe:false , posX:#{x} , posY:#{y}}" if y == map_y

        cpt += 1

	end

    puts "	]," if x != map_x
    puts "	]" if x == map_x

end

puts "];"
