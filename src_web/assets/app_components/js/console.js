
simuApp.controller('consoleController', function($scope,SimuFactory) {

	$scope.$watch( function() { return SimuFactory.getLogs();},function (newVal,oldVal){
        if(typeof newVal !== 'undefined'){
            $scope.logs = SimuFactory.getLogs();

        }
    })

	$scope.ajouterLog = function(obj_log) {
		SimuFactory.addLog(obj_log);
	}

	$scope.nettoyerLog = function() {
		SimuFactory.resetLogs();
	}
});

function ajouter_log(obj_log) {

	/* 
	 * Permet d'afficher et sauvegarder les logs dans un objet (qui pourra ensuite être converti en JSON et envoyé au serveur)
	 * les logs peuvent être du type suivant :
	 * {type:'robotDeplacement', detail:{timer:INT, timestamp:INT, robotID:INT, oldPosX:INT, oldPosY:INT, newPosX:INT, newPosY:INT}}
	 * {type:'robotEteindreIncendie', detail:{timer:INT, timestamp:INT, robotID:INT, puissanceExtinction:INT, feuID:INT}}
	 * {type:'incendieExtinction', detail:{timer:INT, timestamp:INT, feuID:INT, oldPuissance:INT, newPuissance:INT, posX:INT, posY:INT}}
	 */
	var scope = angular.element($("#part_2_left_bottom")).scope();
    scope.$apply(function() {
    	scope.ajouterLog(obj_log);
    });

}

function nettoyer_log() {

	/*
	 * Permet de vider les logs
	 */
	var scope = angular.element($("#part_2_left_bottom")).scope();
    scope.$apply(function() {
    	scope.nettoyerLog();
    });

}

simuApp.factory('SimuFactory', function () {
    var logs = [];
 
    return {
        getLogs:function() {
            return logs;
        },
        addLog:function(log) {
            logs.push(log);
            setTimeout(function(){
				var elem = document.getElementById('part_2_left_bottom');
				// une fois que le log est ajouté, on fait descendre le scroll
				// pour toujours voir les derniers logs
		  		elem.scrollTop = elem.scrollHeight+10;
		  	}, 5);
        },
        resetLogs:function() {
            logs = [];
        }
    };
});