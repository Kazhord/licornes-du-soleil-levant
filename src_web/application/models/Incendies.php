<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Incendies extends CI_Model {

    private $table   = 'incendies';

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*****************************/
    /********* INCENDIES *********/
    /*****************************/
    
    public function getIncendies()
    {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getIncendie($idFeu)
    {
        $this->db->where('idFeu', $idFeu);
        $query = $this->db->get($this->table);
        if ($query->num_rows() == 1) return $query->row();
        return FALSE;
    }

    public function addIncendie($puissance) {
        $b = $this->db->insert($this->table, array(
            'puissance'   => $puissance
        ));

        if($b) {
            return $this->db->insert_id();
        }
        else {
            return -1;
        }
    }

    public function deleteIncendie($idFeu) {
        $this->db->where('idFeu', $idFeu);
        $this->db->delete($this->table);
        return $this->db->affected_rows() == 1;
    }

    public function updateIncendie($idFeu, $puissance) {
        $this->db->set('puissance', $puissance);
        $this->db->where('idFeu', $idFeu);

        $this->db->update($this->table);
        return $this->db->affected_rows() > 0;
    }



}