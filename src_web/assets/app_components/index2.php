<!DOCTYPE HTML>
<html lang="en" >
  <head>
    <meta charset="UTF-8">
    <title>PathFinding.js</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css"/>
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="./transpose.js"></script>
    <script type="text/javascript" src="./angular.min.js"></script>
	<script type="text/javascript" src="./pathfinding-browser.min.js"></script>
    <script type="text/javascript" src="./test.js"></script>
  </head>
<body >
  <div ng-app="algosApp" ng-controller="ControleurAlgo">
      <div>
        <div class=""  style='margin:auto;'>
            <table>
              <tr ng-repeat="row in matrixComplete">
                <td ng:class="{true:'disabled', false:''}[value.occupe ==true]" id="{{value.posX}}{{value.posY}}" ng-repeat="value in row track by $index">
                    {{ value.id }}
                </td>
              </tr>
            </table>
        </div>
      </div>
      
      
      <div class="container">
        <select 
            ng-change="selectAction()"
            ng-model="select_algo" >
          <optgroup label="Algorithmes sans diagonale">
            <option value="a_star">A*</option>
            <option value="disjkra">Disjkra</option>
          </optgroup>
          <optgroup label="Algorithmes avec diagonale">
            <option value="a_star_diag">A*</option>
            <option value="disjkra_diag">Disjkra</option>
          </optgroup>
        </select>
      </div>
   </div>
</body>
</html>

<script>  
      
  
</script>