<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Titre de la page</title>
  <!-- CSS_MIN -->

  <link rel="stylesheet" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/bower_components/onepage-scroll/onepage-scroll.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/app_components/css/general.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/app_components/css/matrice.css') ?>">
  
</head>

<body>

  <div class="main" ng-app="simuApp">
    <section id="part_1">
    	<div class="row">
    		<div class="col-xs-12 text-center">
    			<h1>Gestion d'incendies grâce à des robots</h1>
    			<h2>Simulation interactive</h2>
    		</div>
    	</div>
    	
    	<div id="config" class="row">.
            <div  ng-controller="ControleurAlgo">
            <div id="config_cartes" class="col col-xs-6">
                <div>
                    <div class="text-center"><h3>Cartes</h3></div>
                    <hr />

                    <input type="file" id="files" />
                    <div id="img"></div>
                    <canvas id="canvas"></canvas>
                </div>
            </div>
            </div>
    		<div id="config_robots" class="col col-xs-6">
    			<div>
    				<div class="text-center"><h3>Robots</h3></div>
    				<hr />
	    		</div>
    		</div>
    		<div id="config_incendies" class="col col-xs-6">
    			<div>
    				<div class="text-center"><h3>Incendies</h3></div>
    				<hr />
	    			<div ng-controller="incendieController">

	    				<h3>Ajouter un incendie</h3>
	    				<label for="config_indendie_ajout_puissance">Puissance : </label>
	    				<input type="number" value="0" name="config_indendie_ajout_puissance" id="config_indendie_ajout_puissance" ng-model="newIncendie.puissance" />
	    				<input type="button" value="Ajouter" ng-click="ajouterIncendie()" />

	    				<br />

	    				<h3>Incendies disponibles</h3>
	    				<table id="incendies_disponibles_config" class="table">
							<tr>
								<th>ID</th>
								<th>Puissance</th>
								<th>Supprimer</th>
							</tr>
							<tr ng-repeat="incendie in incendies | reverse">
								<td>{{ incendie.idFeu }}</td>
								<td>{{ incendie.puissance }}</td>
								<td><i class="fa fa-times supprimer" ng-click="supprimerIncendie(incendie)"></i></td>
							</tr>
						</table>

	    			</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section id="part_2">
    	<div id="part_2_left">
    		<div id="part_2_left_top">
                <div id="matrice">
                    <div  ng-controller="ControleurAlgo">
                        <div class="" style='margin:auto;'>
                            <table>
                                <tr ng-repeat="row in matrixComplete">
                                    <td ng:class="{true:'disabled', false:''}[value.occupe == true]" id="{{value.id}}" ng-repeat="value in row track by $index">
                                        <span data-toggle="tooltip" data-placement="right" title="{{value.posX}},{{value.posY}}">.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
   
    		<div id="part_2_left_bottom" ng-controller="consoleController">
    			<ul>
    				<li ng-repeat="log in logs"><span >{{
    					log.type=='incendieExtinction' ?
	    					'Extinction du feu '+log.detail.feuID+'. Sa puissance passe de '+log.detail.oldPuissance+' à '+log.detail.newPuissance+'.'
	    					: (
	    						log.type=='robotDeplacement' ?
	    						'Le robot '+log.detail.robotID+' se déplace de X:'+log.detail.oldPosX+', Y:'+log.detail.oldPosY+' vers X:'+log.detail.newPosX+', Y:'+log.detail.newPosY+'.'
	    						: (
	    							log.type=='robotEteindreIncendie' ?
	    							'Le robot '+log.detail.robotID+' envoie de l\'eau sur le feu '+log.detail.feuID+' (puissance : '+log.detail.puissanceExtinction+').'
	    							: ''
	    						  )
	    					  )
    				}}</span></li>
    			</ul>
    		</div>
    		<!-- @Console : Veuillez lancer une simulation pour voir les étapes au fur et à mesure. -->
    	</div>


    	<div id="part_2_right">
    		
    		<!-- carte & algo -->
    		<div class="row">
    			<div class="col-xs-6">
    				<label for="select_carte">Choix de la carte</label>
		    		<select id="select_carte" class="form-control">
					  <option>1</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					  <option>5</option>
					</select>
    			</div>

    			<div class="col-xs-6"  ng-controller="ControleurAlgo">
    				<label for="select_algo">Choix de l'algorithme</label>
		    		 <select  class="form-control"
                        ng-change="selectAction()"
                        ng-model="select_algo" >
                        <optgroup label="Algorithmes sans diagonale">
                            <option value="a_star">A*</option>
                            <option value="disjkra">Disjkra</option>
                        </optgroup>
                        <optgroup label="Algorithmes avec diagonale">
                            <option value="a_star_diag">A*</option>
                            <option value="disjkra_diag">Disjkra</option>
                        </optgroup>
                     </select>
    			</div>
    		</div>
    		
			<br />

			<!-- robots -->
    		<div class="row">
    			<div class="col-12-xs" ng-controller="robotController">

    				<label for="robots_disponibles">Robots disponibles</label>
    				<table id="robots_disponibles" class="table">
						<tr>
							<th>ID</th>
							<th>Type</th>
							<th>L/S</th>
							<th>Diff terrain max</th>
							<th>Ajouter</th>
						</tr>
						<tr ng-repeat="robot in robots">
							<td>{{ robot.id }}</td>
							<td>{{ robot.type }}</td>
							<td>{{ robot.debit }}</td>
							<td>{{ robot.max_difficulte_terrain }}</td>
							<td><input type="button" value="Ajouter" ng-click="ajouterRobotOnMap()" /></td>
						</tr>
					</table>
    			</div>
    		</div>

    		<!-- incendies -->
    		<div class="row">
    			<div class="col-12-xs" ng-controller="incendieController">

    				<label for="incendies_disponibles">Incendies disponibles</label>
    				<table id="incendies_disponibles" class="table">
						<tr>
							<th>ID</th>
							<th>Puissance</th>
						</tr>
						<tr ng-repeat="incendie in incendies">
							<td>{{ incendie.idFeu }}</td>
							<td>{{ incendie.puissance }}</td>
						</tr>
					</table>
    			</div>
    		</div>

    	</div>

    	<!--
    	<div id="part_2_top">
    		
	    	<div id="part_2_top_left">

	    	</div>
    		
	    	<div id="part_2_top_right">

	    		<div class="row">
	    			<div class="col-xs-6">
	    				<label for="select_carte">Choix de la carte</label>
			    		<select id="select_carte" class="form-control">
						  <option>1</option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>
	    			</div>

	    			<div class="col-xs-6">
	    				<label for="select_algo">Choix de l'algorithme</label>
			    		<select id="select_algo" class="form-control">
						  <option>1</option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>
	    			</div>
	    		</div>
	    		
				<br />

	    		<div class="row">
	    			<div class="col-12-xs">

	    				<label for="robots_disponibles">Robots disponibles</label>
	    				<table id="robots_disponibles" class="table">
							<tr>
								<th>a</th>
								<th>a</th>
								<th>a</th>
								<th>a</th>
							</tr>
							<tr>
								<td>a</td>
								<td>a</td>
								<td>a</td>
								<td>a</td>
							</tr>
						</table>
	    			</div>
	    		</div>

	    		<div class="row">
	    			<div class="col-12-xs">

	    				<label for="incendies_disponibles">Incendies disponibles</label>
	    				<table id="incendies_disponibles" class="table">
							<tr>
								<th>a</th>
								<th>a</th>
								<th>a</th>
								<th>a</th>
							</tr>
							<tr>
								<td>a</td>
								<td>a</td>
								<td>a</td>
								<td>a</td>
							</tr>
						</table>
	    			</div>
	    		</div>

	    	</div>

    	</div>

    	<div id="part_2_bottom">
    		test 2<br />
    	</div>
    	-->
    </section>
  </div>

  <!-- JS_MIN -->
  <script type="text/javascript" src="<?= base_url('assets/bower_components/jquery/jquery.min.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/bower_components/angular/angular.min.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/bower_components/angular-route/angular-route.min.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/bower_components/7c526e6729fee936acde/jquery.onepage-scroll.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>

  <script type="text/javascript" src="<?= base_url('assets/app_components/js/variables.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/general.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/robot.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/incendie.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/console.js') ?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/lib/pathfinding.min.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/lib/transpose.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/carte.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/app_components/js/matrice.js')?>"></script>

</body>

</html>

