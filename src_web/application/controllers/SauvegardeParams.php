<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SauvegardeParams extends CI_Controller {

	private $nbParametres = 0;

	public function __construct() {
		parent::__construct();
		$this->load->model('sauvegarde');

		$this->nbParametres = $this->uri->total_segments() - 2;
		
	}

	public function index() {
		$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
	}

	// /sauvegardeParams/creerParamPost/
	public function creerParamPost() {
		if($post = $this->input->post('some_data')) {
			$arr = json_decode($post);
			$nom = $arr['nom'];

			$idParam = $this->sauvegarde->addParam($nom, $post);

			$this->output
			     ->set_content_type('application/json')
			     ->set_output(json_encode(array('idParam' => $idParam)));

		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}

	}

	// /sauvegardeParams/listeParams/
	public function listeParams() {
		//var_dump($this->sauvegarde->getParams());
		$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($this->sauvegarde->getParams()));
	}

	// /sauvegardeParams/voirParam/$idParam/
	public function voirParam($idParam) {
		$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($this->sauvegarde->getParam($idParam)));
	}


}
/* End of file sauvegardeParams.php */
/* Location: ./application/controllers/sauvegardeParams.php */