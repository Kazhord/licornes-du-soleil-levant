<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Robots extends CI_Model {

    private $table   = 'robots';
    private $table2  = 'typeRobot';

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    /*****************************/
    /*********** ROBOTS **********/
    /*****************************/

    public function getRobots()
    {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getRobot($idRobot)
    {
        $this->db->where('idRobot', $idRobot);
        $query = $this->db->get($this->table);
        if ($query->num_rows() == 1) return $query->row();
        return FALSE;
    }


    public function addRobot($nom, $debit, $idType) {
        $b = $this->db->insert($this->table, array(
            'nom'      => $nom,
            'debit'    => $debit,
            'idType'   => $idType
        ));

        if($b) {
            return $this->db->insert_id();
        }
        else {
            return -1;
        }

    }

    public function deleteRobot($idRobot) {
        $this->db->where('idRobot', $idRobot);
        $this->db->delete($this->table);
        return $this->db->affected_rows() == 1;
    }


    public function updateRobot($idRobot, $nom, $debit, $idType) {
        $this->db->set('nom', $nom);
        $this->db->set('debit', $debit);
        $this->db->set('idType', $idType);
        $this->db->where('idRobot', $idRobot);

        $this->db->update($this->table);
        return $this->db->affected_rows() > 0;
    }


    /*****************************/
    /******** TYPES ROBOT ********/
    /*****************************/


    public function getTypesRobot()
    {
        $query = $this->db->get($this->table2);
        return $query->result();
    }

    public function getTypeRobot($idType)
    {
        $this->db->where('idType', $idType);
        $query = $this->db->get($this->table2);
        if ($query->num_rows() == 1) return $query->row();
        return FALSE;
    }

    public function addTypeRobot($nom, $vitesse, $capacite) {
        $b = $this->db->insert($this->table2, array(
            'nom'        => $nom,
            'vitesse'    => $vitesse,
            'capacite'   => $capacite
        ));

        if($b) {
            return $this->db->insert_id();
        }
        else {
            return -1;
        }
    }

    public function updateTypeRobot($idType, $nom, $vitesse, $capacite) {
        $this->db->set('nom', $nom);
        $this->db->set('vitesse', $vitesse);
        $this->db->set('capacite', $capacite);
        $this->db->where('idType', $idType);

        $this->db->update($this->table2);
        return $this->db->affected_rows() > 0;
    }

    public function deleteTypeRobot($idType) {
        $this->db->where('idType', $idType);
        $this->db->delete($this->table2);
        return $this->db->affected_rows() == 1;
    }



}