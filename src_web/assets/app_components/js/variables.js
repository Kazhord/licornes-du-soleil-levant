var timer = 5000,
base_url,
site_url,
pathname = window.location.pathname,
path = pathname.split('/');
var args = "";
for (var i = 1; i <= path.length - 2; i++) {
	args += "/" + path[i];
}
base_url = "http://" + window.location.host + args + "/";


site_url = base_url + 'index.php/';

// fonction permettant d'ajouter une fonction jquery pour savoir si un div est scrollable
(function($) {
	$.fn.hasScrollBar = function() {
		return this.get(0).scrollHeight > this.height();
	}
})(jQuery);