<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sauvegarde extends CI_Model {

    private $table_params   = 'sauv_params';

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*****************************/
    /********* INCENDIES *********/
    /*****************************/
    
    public function getParams()
    {
        $query = $this->db->get($this->table_params);
        return $query->result();
    }

    public function getParam($idParam)
    {
        $this->db->where('idParam', $idParam);
        $query = $this->db->get($this->table_params);
        if ($query->num_rows() == 1) return $query->row();
        return FALSE;
    }

    public function addParam($nom, $post) {
        $b = $this->db->insert($this->table_params, array(
            'nom'   => $nom,
            'contenu' => $post
        ));

        if($b) {
            return $this->db->insert_id();
        }
        else {
            return -1;
        }
    }

}