
var matrice = [];
var image;
var taille;

//Chargement du fichier image
document.getElementById('files').addEventListener('change', handleFileSelect, false);
function handleFileSelect(evt) {
    var files = evt.target.files;
    var f = files[0];

    var reader = new FileReader();

    reader.onload = (function(theFile) {
        return function(e) {
            var img = new Image();
            img.onload = function(){
                var canvas = document.getElementById('canvas');
                canvas.width = img.width;
                canvas.height = img.height;

                var ctx = canvas.getContext('2d');
                ctx.drawImage(img, 0, 0, img.width, img.height);

                var pix = canvas.getContext('2d').getImageData(0, 0, img.width, img.height).data;

                var nb_rows = img.width;
                var nb_cols = img.height;
                var nb_pix = pix.length;
                taille = [nb_cols, nb_rows];

                var black_pixel = [];
                for (var j = 0, p = 0 ; j < nb_pix; j += 4, p++) {
                    black_pixel[p] = pix[j];
                }

                for (var r = 0, p = 0; r < nb_rows; r++, p+=nb_rows){
                    row_pix = black_pixel.splice(0,img.height);
                    row  = [];
                    //console.log(p)
                    for (var pixel = 0; pixel < nb_cols; pixel++){
                        row.push({id:p + pixel, difficulte_terrain:row_pix[pixel], occupe:false, posX:pixel, posY:r})
                    }
                    matrice.push(row)
                }

                //console.log(matrice)

            };

            img.src = e.target.result;
        };
    })(f);

    reader.readAsDataURL(f);
}

function getMatriceDifficulte(){
    return matrice;
}

function getTaille(){
    return taille;
}