

simuApp.controller('incendieController', function($scope, $rootScope, $http) {

	$rootScope.incendies = [];

	$rootScope.newIncendie = {
	    puissance: 0
	}

	$rootScope.ajouterIncendie = function() {
		$http.get(site_url+'gestionnaireFeu/creerIncendie/'+$rootScope.newIncendie.puissance).
		success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available
			$rootScope.getListeIncendies();
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.log("Un problème est survenu. (ajouterIncendie).");
		});
	}

	$rootScope.supprimerIncendie = function(incendie) {
		$http.get(site_url+'gestionnaireFeu/supprimerIncendie/'+incendie.idFeu).
		success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available
			$rootScope.getListeIncendies();
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.log("Un problème est survenu. (supprimerIncendie).");
		});
	}

	$rootScope.getListeIncendies = function() {
		$http.get(site_url+'gestionnaireFeu/listeIncendies').
		success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available

			$rootScope.incendies = data;
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.log("Un problème est survenu. (getListeIncendies).");
		});
	}

	$rootScope.getListeIncendies();

});