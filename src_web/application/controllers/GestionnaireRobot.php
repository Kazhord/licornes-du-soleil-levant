<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GestionnaireRobot extends CI_Controller {

	private $nbParametres = 0;

	public function __construct() {
		parent::__construct();
		$this->load->model('robots');

		$this->nbParametres = $this->uri->total_segments() - 2;
		
	}


	public function index()
	{
		$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
	}
	
	// /gestionnaireRobot/listeRobots/
	public function listeRobots() {
		if($this->nbParametres == 0) {
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($this->robots->getRobots()));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}


	}
	
	// /gestionnaireRobot/creerRobot/$nom/$debit/$idType/
	public function creerRobot($nom = "", $debit = "", $idType = "") {
		if($this->nbParametres == 3) {
			$idRobot = $this->robots->addRobot($nom, $debit, $idType);
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('idRobot' => $idRobot)));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}

	}

	// /gestionnaireRobot/supprimerRobot/$idRobot/
	public function supprimerRobot($idRobot = "") {
		if($this->nbParametres == 1) {
			if ($this->robots->deleteRobot($idRobot)) {
				$bool = TRUE;
			}
			else {
				$bool = FALSE;
			}


			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('success' => $bool)));

		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	
	}

	// /gestionnaireRobot/modifierRobot/$idRobot/$nom/$debit/$idType/
	public function modifierRobot($idRobot, $nom, $debit, $idType) {
		if($this->nbParametres == 4) {
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('success' => $this->robots->updateRobot($idRobot, $nom, $debit, $idType))));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}

	// /gestionnaireRobot/voirRobot/$idRobot/
	public function voirRobot($idRobot = "") {
		if($this->nbParametres == 1) {
			if($robot = $this->robots->getRobot($idRobot)) {
				$this->output
		     	 ->set_content_type('application/json')
		     	 ->set_output(json_encode($robot));
			}
			else {
				$this->output
		     	 ->set_content_type('application/json')
		     	 ->set_output(json_encode(array('success' => FALSE)));
			}
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}


	// ___________________________________________________

	// /gestionnaireRobot/listeTypesRobot/
	public function listeTypesRobot() {
		if($this->nbParametres == 0) {
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($this->robots->getTypesRobot()));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}


	}
	
	// /gestionnaireRobot/creerTypeRobot/$nom/$vitesse/$capacite/
	public function creerTypeRobot($nom = "", $vitesse = "", $capacite = "") {
		if($this->nbParametres == 3) {
			$idType = $this->robots->addTypeRobot($nom, $vitesse, $capacite);
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('idType' => $idType)));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}

	}

	// /gestionnaireRobot/supprimerTypeRobot/$idType/
	public function supprimerTypeRobot($idType = "") {
		if($this->nbParametres == 1) {
			if ($this->robots->deleteTypeRobot($idType)) {
				$bool = TRUE;
			}
			else {
				$bool = FALSE;
			}


			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('success' => $bool)));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	
	}

	// /gestionnaireRobot/modifierTypeRobot/$idType/$nom/$vitesse/$capacite/
	public function modifierTypeRobot($idType, $nom, $vitesse, $capacite) {
		if($this->nbParametres == 4) {
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('success' => $this->robots->updateTypeRobot($idType, $nom, $vitesse, $capacite))));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}

	// /gestionnaireRobot/voirTypeRobot/$idType/
	public function voirTypeRobot($idType = "") {
		if($this->nbParametres == 1) {
			if($type = $this->incendies->getTypeRobot($idType)) {
				$this->output
		     	 ->set_content_type('application/json')
		     	 ->set_output(json_encode($type));
			}
			else {
				$this->output
		     	 ->set_content_type('application/json')
		     	 ->set_output(json_encode(array('success' => FALSE)));
			}
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}

}

/* End of file gestionnaireRobot.php */
/* Location: ./application/controllers/gestionnaireRobot.php */