<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GestionnaireFeu extends CI_Controller {

	private $nbParametres = 0;

	public function __construct() {
		parent::__construct();
		$this->load->model('incendies');

		$this->nbParametres = $this->uri->total_segments() - 2;
		
	}

	public function index() {
		$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
	}

	// /gestionnaireFeu/listeIncendies/
	public function listeIncendies() {
		if($this->nbParametres == 0) {
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode($this->incendies->getIncendies()));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}

	// /gestionnaireFeu/creerIncendie/$puissance/
	public function creerIncendie($puissance = "") {
		if($this->nbParametres == 1) {
			$idFeu = $this->incendies->addIncendie($puissance);
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('idFeu' => $idFeu)));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}

	// /gestionnaireFeu/supprimerIncendie/$idFeu/
	public function supprimerIncendie($idFeu = "") {
		if($this->nbParametres == 1) {
			if ($this->incendies->deleteIncendie($idFeu)) {
				$bool = TRUE;
			}
			else {
				$bool = FALSE;
			}

			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('success' => $bool)));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
		

	}

	// /gestionnaireFeu/modifierIncendie/$idFeu/$puissance/
	public function modifierIncendie($idFeu = "", $puissance = "") {
		if($this->nbParametres == 2) {
			$this->output
		     ->set_content_type('application/json')
		     ->set_output(json_encode(array('success' => $this->incendies->updateIncendie($idFeu, $puissance))));
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}

	// /gestionnaireFeu/voirIncendie/$idFeu/
	public function voirIncendie($idFeu = "") {
		if($this->nbParametres == 1) {
			if($incendie = $this->incendies->getIncendie($idFeu)) {
				$this->output
		     	 ->set_content_type('application/json')
		     	 ->set_output(json_encode($incendie));
			}
			else {
				$this->output
		     	 ->set_content_type('application/json')
		     	 ->set_output(json_encode(array('success' => FALSE)));
			}
		}
		else {
			$this->output
		     ->set_content_type('text/html')
		     ->set_output("Invalid parameters");
		}
	}
}

/* End of file gestionnaireFeu.php */
/* Location: ./application/controllers/gestionnaireFeu.php */